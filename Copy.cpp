#include <iostream>
#include <vector>
#include <numeric>
#include <unordered_map>
#include <algorithm>

int main() {
	std::vector<int> from_vector(10);
	std::iota(from_vector.begin(), from_vector.end(), 0);
	std::vector<int> to_vector;
	std::copy(from_vector.begin(), from_vector.end(),
		std::back_inserter(to_vector)); // std::copy ��� ������ �������� �����������, ��� � ������� to_vector = from_vector
	for (auto elem : to_vector) {
		std::cout << elem << '\t';
	}
	std::cout << "\n";

	std::unordered_map<std::string, std::string> u = { {"red", "#FF0000"}, {"green", "#00FF00"}, {"blue", "#0000FF"} };
	int n1 = std::count_if(u.begin(), u.end(), [](auto i) {return i.first == "red"; });
	std::cout << "number of reds: " << n1 << '\n';

	std::vector<int> v(10);
	std::iota(v.begin(), v.end(), 0);
	std::fill(v.begin(), v.end(), -1);
	for (auto elem : v) {
		std::cout << elem << " ";
	}
	std::cout << '\t';
}